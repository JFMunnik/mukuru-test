﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTestExample.App_Global
{
    public class GlbSessionVariables
    {
        public static bool ValidSession { get; set; }

        public static string LoggedInUserName { get; set; }

        public static int LoggedInUserID { get; set; }

        //Set Admin UserID to obscure number
        public static int AdminUserID = -55;

        public static string WebsiteUrl = "https://localhost:44379/";
    }
}