﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Data.SqlClient;
using WebTestExample.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WebTestExample.App_Start
{
    public class DboConnectionConfig : DbContext
    {
        public DboConnectionConfig() : base(ConnectionString("mukuru")) { }

        //Set Precision otherwise loose point values when inserting to dbo
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention(38, 18));
        }

        //Declare models and assign table / model names
        // public DbSet<UserModel> ==> Model name as defines ,  User ==> Table name  , { get; set; }
        //User Table
        public DbSet<UserModel> User { get; set; }

        //Currency Table
        public DbSet<CurrencyModel> Currency { get; set; }

        //Order Table
        public DbSet<OrderModel> Order { get; set; }

        //Build sql String
        private static string ConnectionString(string dbo)
        {
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

            /* Mukuru Dbo */
            if (dbo == "mukuru")
            {
                sqlBuilder.DataSource = @"(LocalDb)\MSSQLLocalDB";
                sqlBuilder.InitialCatalog = "Mukuru";
                sqlBuilder.UserID = "Mukuru";
                sqlBuilder.Password = "Mukuru";
                sqlBuilder.PersistSecurityInfo = true;
                sqlBuilder.MultipleActiveResultSets = true;
            }
            /* Check if server can be connected to */

            IsServerConnected(sqlBuilder.ToString());

            /* To String value to return it properly */

            return sqlBuilder.ToString(); ;
        }

        private static void IsServerConnected(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    //Check if connection is possible
                    connection.Open();
                }
                catch (SqlException)
                {
                    //Throw error when connection to db was unsuccesfull
                    throw new Exception("Application could not connect to database please contact the administrator");
                }
            }
        }
    }
}