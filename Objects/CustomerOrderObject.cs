﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTestExample.Objects
{
    public class CustomerOrderObject
    {
       public List<OrderCurrencyObject> OrderCurrencyList { get; set; }
       public int UserID { get; set; }
       public DateTime OrderCreateDate { get; set; }
    }
}