﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTestExample.Objects
{
    public class OrderCurrencyObject
    {
        public int CurrencyID { get; set; }
        public decimal OrderValueForeignCurr { get; set; }
        public decimal OrderValueNativeCurr { get; set; }
    }
}