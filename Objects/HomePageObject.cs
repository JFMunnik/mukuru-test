﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebTestExample.Models;

namespace WebTestExample.Objects
{
    public class HomePageObject
    {
        public List<CurrencyModel> CurrencyList { get; set; }
    }
}