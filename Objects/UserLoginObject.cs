﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTestExample.Objects
{
    public class UserLoginObject
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}