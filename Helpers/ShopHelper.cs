﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web;
using WebTestExample.App_Global;
using WebTestExample.App_Start;
using WebTestExample.Models;

namespace WebTestExample.Helpers
{
    public class ShopHelper
    {
        /// <summary>  
        ///  This function Calls the appropriate additional functions based on currency Code.  
        ///  <param name="CurrencyModel">the CurrencyModel</param>
        ///  <returns>void</returns>
        /// </summary>  
        public void AdditionalAction(CurrencyModel currencyObj)
        {
            try
            {
                //Initialise new dbo connection
                var _Db = new DboConnectionConfig();

                if (currencyObj.CurrencyCode == "GBP")
                {
                    string userSendEmail = _Db.User.Where(w => w.UserID == GlbSessionVariables.LoggedInUserID).Select(s => s.Email).FirstOrDefault();
                    if (userSendEmail != null)
                    {
                        SendEmailViaWebApi(userSendEmail);
                    }

                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>  
        ///  This function Calculates the actual amount that needs to be paid.  
        ///  Paramenters : CurrencyModel , decimal(ForeignCurrValue) , decimal(NativeCurrValue)
        ///  <param name="CurrencyModel">the CurrencyModel</param>
        ///  <param name="ForeignCurrValue">the ForeignCurrValue decimal</param>
        ///  <param name="NativeCurrValue">the NativeCurrValue decimal</param>
        ///  
        ///  <returns>AmountToBePaid</returns>
        /// </summary>  
        public decimal CalcAmountToBePaid(CurrencyModel currencyObj, decimal foreignCurrValue, decimal nativeCurrValue)
        {
            decimal returnAmountToBePaid = new decimal();
            decimal exhangeRateAmount = new decimal();
            try
            {
                //Switch between the two types either foreign or local currency calculations
                if (foreignCurrValue != 0)
                {
                    exhangeRateAmount = (foreignCurrValue / currencyObj.ExchangeRate); // Calculate exchangeRate first
                    returnAmountToBePaid = exhangeRateAmount + (exhangeRateAmount * currencyObj.SurCharge); // Then calculate acutal ammount to be paid
                }
                else if (nativeCurrValue != 0)
                {
                    exhangeRateAmount = (foreignCurrValue * currencyObj.ExchangeRate);// Calculate exchangeRate first
                    returnAmountToBePaid = exhangeRateAmount + (exhangeRateAmount * currencyObj.SurCharge);// Then calculate acutal ammount to be paid
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return returnAmountToBePaid; //Return Actual ammount to be paid
        }

        /// <summary>  
        ///  This function sends the email through gmails smtp.   
        ///  Parameters String userEmail
        ///  <param name="userEmail">the string foruserEmail</param>
        ///  Return void
        /// </summary>  
        private void SendEmailViaWebApi(string userEmail)
        {
            try
            {
                //Configure email contents
                string subject = "Order";
                string body = "Your order was palced successfully";
                string FromMail = "mukuru.smtp@gmail.com";
                string emailTo = userEmail;

                MailMessage mail = new MailMessage();
                mail.To.Add(emailTo);
                //mail.To.Add("Another Email ID where you wanna send same email");
                mail.From = new MailAddress(FromMail);
                mail.Subject = subject;
                mail.Body = body;

                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential
                     ("mukuru.smtp@gmail.com", "mukuru5015");
                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>  
        ///  Syncs currency values    
        ///  <returns>HttpResponseMessage</returns>
        /// </summary>  
        public static HttpResponseMessage SetCurrencyValuesJsonRates()
        {
            try
            {
                const string URL = "http://www.apilayer.net/api/live";
                string source = "&source=ZAR";
                string currencies = "&currencies=USD,GBP,EUR,KES";
                string accessKey = "?access_key=96c3bd7fb8853be209e8cf6c7bb083dc";
                string format = "&format=1";
                //Can only send key , cannot change source
                string urlParameters = accessKey;

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                if (response.IsSuccessStatusCode)
                {
                    //TODO : Update dbo currency values with the response
                    //On purpose not updateing because the values will be inccorrect.
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                }

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();

                return response;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}