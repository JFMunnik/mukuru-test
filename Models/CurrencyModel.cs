﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebTestExample.Models
{
    [Table("Currency")]
    public class CurrencyModel
    {
        [Key]
        [Column("CurrencyID")]
        public int CurrencyID { get; set; }

        [Column("CurrencyCode")]
        public string CurrencyCode { get; set; }

        [Column("CurrencyName")]
        public string CurrencyName { get; set; }

        [Column("ExchangeRate")]
        public decimal ExchangeRate { get; set; }

        [Column("SurCharge")]
        public decimal SurCharge { get; set; }

        [Column("Discount")]
        public decimal Discount { get; set; }
    }
}