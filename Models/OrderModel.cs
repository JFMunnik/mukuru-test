﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebTestExample.Models
{
    [Table("Order")]
    public class OrderModel
    {
        [Key]
        [Column("OrderID")]
        public int OrderID { get; set; }

        [Column("CurrencyID")]
        public int CurrencyID { get; set; }

        [Column("ExchangeRate")]
        public decimal ExchangeRate { get; set; }

        [Column("SurCharge")]
        public decimal SurCharge { get; set; }

        [Column("AmountToBePaid")]
        public decimal AmountToBePaid { get; set; }

        [Column("OrderCreateDate")]
        public DateTime OrderCreateDate { get; set; }

        [Column("Discount")]
        public decimal Discount { get; set; }

        [Column("UserID")]
        public int UserID { get; set; }
        
    }
}