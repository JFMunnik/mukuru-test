﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTestExample.Models
{
    public class OrderFormModel
    {
        public int CurrencyID { get; set; }

        public decimal ForeignCurrValue { get; set; }

        public decimal NativeCurrValue { get; set; }
    }
}