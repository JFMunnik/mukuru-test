﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebTestExample.Models
{
    public class RegistrationFormModel
    {
        [Required]
        [MaxLength(40)]
        public string Email { get; set; }
        [Required]
        [MaxLength(40)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(40)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(40)]
        public string Password { get; set; }
        [Required]
        [MaxLength(40)]
        public string ConfirmPassword { get; set; }
    }
}