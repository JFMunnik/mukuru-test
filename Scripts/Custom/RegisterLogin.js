﻿var getSomeData = function () {
    var data;

    $.ajax({
        url: '/data/people.json',
        dataType: 'json',
        success: function (resp) {
            data = resp.people;
        }
    });

    return data;
}

