﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebTestExample.App_Global;
using WebTestExample.App_Start;
using WebTestExample.Helpers;
using WebTestExample.Models;
using WebTestExample.Objects;

namespace WebTestExample.Controllers
{
    [System.Web.Http.RoutePrefix("api/shop")]

    public class ShopController : ApiController
    {
        public static HomePageObject InitHomeValues()
        {
            HomePageObject returnObject = new HomePageObject();
            //Initialise new dbo connection
            var _Db = new DboConnectionConfig();

            try
            {
                returnObject.CurrencyList = _Db.Currency.ToList();
            }
            catch (Exception e)
            {
                throw e;
            }

            return returnObject;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("Order")]
        [System.Web.Http.Route("Order")]
        public HttpResponseMessage CreateNewOrder(OrderFormModel orderFormModel)
        {
            OrderModel newOrder = new OrderModel();

            //instantiate helper
            ShopHelper shopHelper = new ShopHelper();

            HttpResponseMessage response = new HttpResponseMessage();

            //Initialise new dbo connection
            var _Db = new DboConnectionConfig();
            try
            {
                decimal foreignCurrValue = orderFormModel.ForeignCurrValue;
                decimal nativeCurrValue = orderFormModel.NativeCurrValue;

                DateTime dateNow = DateTime.Now;
                //Get This currncy model
                CurrencyModel currencyObj = _Db.Currency.Where(w => w.CurrencyID == orderFormModel.CurrencyID).FirstOrDefault();

                if (currencyObj == null)
                {
                    throw new Exception("The currency you have selected no longer exists");
                }

                newOrder.CurrencyID = orderFormModel.CurrencyID;

                newOrder.ExchangeRate = currencyObj.ExchangeRate;

                newOrder.SurCharge = currencyObj.SurCharge;

                newOrder.OrderCreateDate = dateNow;

                //Calc AmountToBePaid
                newOrder.AmountToBePaid = shopHelper.CalcAmountToBePaid(currencyObj, foreignCurrValue, nativeCurrValue);

                //Calc Discount
                newOrder.AmountToBePaid = currencyObj.Discount;

                newOrder.UserID = GlbSessionVariables.LoggedInUserID;

                shopHelper.AdditionalAction(currencyObj);

                _Db.Order.Add(newOrder);

                _Db.SaveChanges();

                //TODO: Insert alert to show order was placed successfully
                response = Request.CreateResponse(HttpStatusCode.Redirect);

                response.Headers.Location = new Uri(GlbSessionVariables.WebsiteUrl);
            }
            catch (Exception e)
            {
                throw e;
            }

            return response;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("Currency")]
        [System.Web.Http.Route("currency/update")]
        public HttpResponseMessage UpdateCurrencies()
        {

            //Initialise new dbo connection
            var _Db = new DboConnectionConfig();
        
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                //Check if user is admin 
                if (GlbSessionVariables.AdminUserID == GlbSessionVariables.LoggedInUserID)
                {
                    //Call to Set Currencies exchange rates
                    response = ShopHelper.SetCurrencyValuesJsonRates();
                }
                else
                {
                    //Set user session to expired
                    GlbSessionVariables.ValidSession = false;
                    GlbSessionVariables.LoggedInUserID = 0;
                    GlbSessionVariables.LoggedInUserName = "";
                  
                    //Thow Err of no access
                    throw new Exception("This user does not have access to this api call");

                }
              
            }
            catch (Exception e)
            {
                throw e;
            }
            return response;
        }
    }
}