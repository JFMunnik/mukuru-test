﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebTestExample.App_Start;
using WebTestExample.Helpers;
using WebTestExample.Models;
using WebTestExample.App_Global;
using WebTestExample.Objects;

namespace WebTestExample.Controllers
{
    [System.Web.Http.RoutePrefix("api/user")]
    public class UserController : ApiController
    {
       

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("Create")]
        [System.Web.Http.Route("Create")]
        public HttpResponseMessage CreateNewUser(RegistrationFormModel update)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            UserModel user = new UserModel(); 
            try
            {
                if (ModelState.IsValid && update != null)
                {
                    //Set up new Dbo Connection
                    var _Db = new DboConnectionConfig();
                    PasswordHelper pwdHelper = new PasswordHelper();
               
                    /* Check if email already in system */
                    if (_Db.User.Where(s => s.Email == update.Email).Any())
                    {
                        throw new Exception("Email is already registered in the system.");
                    }

                    user.Email = update.Email;

                    user.UserName = update.Email;

                    user.FirstName = update.FirstName;

                    user.LastName = update.LastName;

                    user.Salt = user.LastName;

                    user.Password = PasswordHelper.Hash(update.Password);

                    _Db.User.Add(user);
                    _Db.SaveChanges();

                    // Create a 201 response.
                    response = Request.CreateResponse(HttpStatusCode.Redirect);

                    response.Headers.Location = new Uri(GlbSessionVariables.WebsiteUrl + "login");

                    return response;
                }
                else
                {
                    return response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("Login")]
        [System.Web.Http.Route("login")]
        public HttpResponseMessage Login(UserLoginObject login)
        {
            HttpResponseMessage response = new HttpResponseMessage();
          
            try
            {
                //Set up new Dbo Connection
                var _Db = new DboConnectionConfig();
                
                var currUser = _Db.User.Where(s => s.Email == login.Email).FirstOrDefault();
                /* Check if user exists*/
                if (currUser == null)
                {
                    throw new Exception("There is no user listed for this email. Please Register an account.");
                }

                //Evalueate password for this account
                var pswIsCorrect = PasswordHelper.Verify(login.Password, currUser.Password);

                if (!pswIsCorrect)
                {
                    throw new Exception("This password / username is incorrect.");
                }

                // Set session valid
                GlbSessionVariables.ValidSession = true;

                //Set Username
                GlbSessionVariables.LoggedInUserName = currUser.UserName;

                GlbSessionVariables.LoggedInUserID = currUser.UserID;

                response = Request.CreateResponse(HttpStatusCode.Redirect);

                response.Headers.Location = new Uri(GlbSessionVariables.WebsiteUrl);

                return response;
               
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}