﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebTestExample.Objects;

namespace WebTestExample.Controllers
{
    public class RoutesController : Controller
    {
        /// <summary>  
        ///  Route to home page and get init data for homepage
        /// </summary>  
        public ActionResult Index()
        {
            try
            {
                ViewBag.Title = "Home Page"; // Set Page Title

                //Check if active valid session is available otherwise redirect to Login page
                if (!App_Global.GlbSessionVariables.ValidSession)
                {
                    Response.RedirectToRoute("Login");
                }
                else
                {
                    //Get initialise Data for home page
                    
                    HomePageObject tempHomeObj = ShopController.InitHomeValues();
                    ViewData["HomePageObject"] = tempHomeObj; // Add init data to viewBag
                    return View();
                }
                
                //Open home page
                return View();
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        /// <summary>  
        /// Route to login page
        /// </summary>  
        public ActionResult Login()
        {
            ViewBag.Title = "Login Page";

            return View();
        }

        /// <summary>  
        /// Route to registration page
        /// </summary>  
        public ActionResult Register()
        {
            ViewBag.Title = "Register Page";

            return View();
        }
    }
}