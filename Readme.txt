mukuru test
# Mukuru Test
This is the assignment that was given to me by Mukuru

## Getting Started

This is a single page web application primarily written in c#. To order currency.

### Prerequisites
Visual studio
Git
Sql server Manager


### Installing

1) Clone this repo git clone https://JFMunnik@bitbucket.org/JFMunnik/mukuru-test.git. 
2) Then open the repo Under ~/Mukuru DBO There is a Mukuru.bacpac flatfile
3) import this flat file and make sure dbo exists
4) Remove the ~/Mukuru DBO folder and nested  Mukuru.bacpac file
4) Build and run application

## Running the tests

Explain how to run the automated tests for this system

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Jan Munnik** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

